﻿extern alias stu3;
extern alias r4;

using System;
using System.Net.Http;

using R4FhirXmlParser = r4::Hl7.Fhir.Serialization.FhirXmlParser;
using R4Bundle = r4::Hl7.Fhir.Model.Bundle;
using R4FhirXmlSerializer = r4::Hl7.Fhir.Serialization.FhirXmlSerializer;
using STU3Resource = stu3::Hl7.Fhir.Model.Resource;
using STU3FhirXmlParser = stu3::Hl7.Fhir.Serialization.FhirXmlParser;

using CHBase.Fhir.R4;
using System.Xml;
using CHBase.SDK;
using CHBase.Fhir.Transformers;
using CHBase.SDK.Web;
using System.Collections.Generic;

namespace R4FhirTransformApp
{
    class Program
    {
        static readonly HttpClient client = new HttpClient();
        static async System.Threading.Tasks.Task Main()
        {
            string fhirApi = "http://microsoft.fhirserver.com/";

            try
            {
                List<string> fhirResources = Resources.GetResourceList();

                foreach (string resource in fhirResources)
                {
                    client.DefaultRequestHeaders.Add("Accept", "application/fhir+xml");
                    HttpResponseMessage response = await client.GetAsync(fhirApi + resource);
                    response.EnsureSuccessStatusCode();
                    string responseString = await response.Content.ReadAsStringAsync();

                    R4FhirXmlParser fxp = new R4FhirXmlParser();
                    R4Bundle bund = fxp.Parse<R4Bundle>(responseString);
                    if (bund != null)
                    {
                        foreach (R4Bundle.EntryComponent ec in bund.Entry)
                        {
                            if (null != ec && null != ec.Resource)
                            {
                                var serializedResource = new R4FhirXmlSerializer().SerializeToString(ec.Resource);

                                R4XmlParser parser = new R4XmlParser(serializedResource, resource);
                                XmlDocument xdoc = parser.ToSTU3();
                                string transformedResourceInXml = xdoc.OuterXml;
                                STU3Resource fhirResource = new STU3FhirXmlParser().Parse<STU3Resource>(transformedResourceInXml);
                                fhirResource.Id = null;
                                HealthRecordItem healthItem = fhirResource.ToCHBase();

                                CreateItemInCHBase(healthItem);
                            }
                        }
                    }
                }

               
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
        }

        static void CreateItemInCHBase(HealthRecordItem healthItem)
        {
            HealthRecordAccessor accessor = GetAccessorForHealthRecord();
            accessor.NewItem(healthItem);
        }

        static HealthRecordAccessor GetAccessorForHealthRecord()
        {
            OfflineWebApplicationConnection connection = new OfflineWebApplicationConnection(AppSettings.PersonId);
            connection.Authenticate();
            HealthRecordAccessor accessor = new HealthRecordAccessor(connection, AppSettings.RecordId);
            return accessor;
        }
    }
}
