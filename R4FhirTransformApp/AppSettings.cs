﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R4FhirTransformApp
{
    public class AppSettings
    {
        public static Guid PersonId
        {
            get
            {
                return new Guid(ConfigurationManager.AppSettings["PersonId"]);
            }
        }

        public static Guid RecordId
        {
            get
            {
                return new Guid(ConfigurationManager.AppSettings["RecordId"]);
            }
        }

        public static Guid ApplicationId
        {
            get
            {
                return new Guid(ConfigurationManager.AppSettings["ApplicationId"]);
            }
        }

        public static string HealthServiceUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["HealthServiceUrl"];
            }
        }

        public static string AppCertSubject
        {
            get
            {
                return ConfigurationManager.AppSettings["AppCertSubject"];
            }
        }
    }
}
