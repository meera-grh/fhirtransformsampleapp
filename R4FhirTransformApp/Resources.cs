﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R4FhirTransformApp
{
    public static class Resources
    {
        public static readonly List<string> resources = new List<string>() 
        { 
            "AllergyIntolerance", 
            "Condition",
            "Encounter",
            "DiagnosticReport",
            "Procedure",
            "Observation"
        };

        public static List<string> GetResourceList()
        {
            return resources;
        }
    }
}
